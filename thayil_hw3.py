import matplotlib.pyplot as plt
import numpy as np
from sys import argv
import math


script, model_smg, ncounts_850 = argv
txt = open(model_smg)
txt2 = open(ncounts_850)


#parsing each line in file
def numDer(x, func, resolution):
	h = resolution
	der = ((2*h)**-1)*func(x+h) - ((2*h)**-1)*func(x-h)
	return der

lum = []
log_lum = []
N = []
for i in range(0,11):
	for line in txt:
		data = line.split()
		lummy = np.float32(data[0])
		lum.append(lummy)
		log_lum.append(math.log10(lummy))
		N.append(np.float32(data[1]))
	#print lum[i]
	#print N[i]
	
log_lum_obs = []
log_dNdL_obs = []
for i in range(0,154):
	for line in txt2:
		data = line.split()
		log_lum_obs.append(np.float32(data[0]))
		log_dNdL_obs.append(np.float32(data[1]))
	
#calculating log dNdL
dNdL = []
log_dNdL = []
for i in range(0,11):
	chgN = N[i] - N[i+1]
	chgL = lum[i] - lum[i+1]
	dNdL.append(chgN/chgL)
	dNdL[i] = -dNdL[i]
	log_dNdL.append(math.log10(dNdL[i]))
	#log_dNdL[i] = -log_dNdL[i]
	#print dNdL[i]
	#print log_dNdL[i]
log_lum.pop()
for i in range(0,11):
	print log_dNdL[i]
	print log_lum[i]

plt.plot(log_dNdL_obs, log_lum_obs, 'g^', log_dNdL, log_lum, 'rs')
plt.xlabel('log_dN/dL')
plt.ylabel('log_luminosity')
plt.show()

	





