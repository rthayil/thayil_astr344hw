import numpy as np
import math
import pylab


def fn(x):
	return np.sin(x)

def gn(x):
	return (x**3)-x-2

def yn(x):
	return -6+x+(x**2)

def numDer(x, func, resolution):
	h = resolution
	der = ((2*h)**-1)*func(x+h) - ((2*h)**-1)*func(x-h)
	return der

tolerance = .00001
x1 = 1.00
x2 = 1.00
func = input('enter a function: fn, gn, or yn: ')

x = np.linspace(-10,10,100)  #plotting functions from -10 to 10
pylab.plot(x,func(x))		
pylab.show()
							#command line interface is a little goofy here, on windows, must close plot and press down arrow to continue
ratio2 = 1
ratio1 = numDer(x1, func, tolerance)/numDer(x2,func,tolerance)
if ratio1 > 0:  							# if the ratio of the derivatives of the funcs are positive, they have the same-signed slope
	while ratio2 >= 0 :
		x1 = input('enter bracket 1: ')		#asking user for brackets
		x2 = input('enter bracket 2: ')
		if math.fabs(func(x1)) < tolerance:  #checking if endpoints are roots
			if math.fabs(func(x2)) < tolerance:
				print ("roots: " , x1, x2)
				ratio2 = -1
			print("root: ", x1)
			ratio2 = -1
		elif math.fabs(func(x2)) < tolerance:
			print ("root: " , x2)
			ratio2 = -1
		else:	
			ratio2 = func(x1)/func(x2)		#if brackets aren't bracketing the roots, ask for 2 new ones

			
if math.fabs(func(x1)) > tolerance and math.fabs(func(x2)) > tolerance:
	midpt = np.float32(x1+(x2-x1)/2)

	while math.fabs(func(midpt)) > tolerance : 				#bisection routine
		xn = midpt
		if func(xn)/func(x2) < 0:
			x1 = xn
		elif func(x1)/func(xn) < 0:
			x2 = xn
		else:
			print "no root"
		midpt = x1 + (x2-x1)/2



	print "root: ", midpt
