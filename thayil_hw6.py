import numpy as np
import math
import pylab
from datetime import datetime
from datetime import timedelta
t1 = datetime.now()

def numDer(x, func, resolution):
	h = resolution
	der = ((2*h)**-1)*func(x+h) - ((2*h)**-1)*func(x-h)
	return der

def NR(x, func):
	return x-(func(x)/numDer(x, func, 100))

def fn(x):
	return np.sin(x)
	
def gn(x):
	return (x**3)-x-2
	
def yn(x):
	return -6+x+(x**2)

def bv(t):
	return (2*(-27)*((10/.087)**3)/100)/(math.exp(-27*(10/.087)/(-16*t))-1) - 1.25*(10**-12)   #not getting 42 as the root, probably a units issue...


tolerance = .0001
func = input('enter a function: fn, gn, or yn: ')
	
#x = np.linspace(-10,10,100)  #plotting functions from -10 to 10
#pylab.plot(x,func(x))		
#pylab.show()   #command line interface is a little goofy here, on windows, must close plot and press down arrow to continue

xi = input('enter a starting x value: ')
iterations = input('enter max number of iterations: ')
				
			
i = 0;

while((math.fabs(func(xi)) >= tolerance and not i > iterations)  or (i > iterations and not math.fabs(func(xi)) >= tolerance)):  #ugly XOR to ensure while loop stops after 1000 iterations
	xi = NR(xi, func)
	i = i+1
	print (i)
	
t2 = datetime.now()	

if (i==iterations+1):
		print "root estimate with ", iterations , "iterations:" , xi, 'Try again with new starting value'
else:
	print "root: " ,xi
	print "interations =", i
print "time elapsed: ", t2-t1
