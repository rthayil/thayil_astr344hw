import matplotlib.pyplot as plt
import numpy as np

x0 = np.float32(0)
x1 = np.float32(5)

def nTrapIntegrate(nTraps, x, y):
	a = 0
	def oneTrapIntegrate (x0, x1, y0, y1):
		chunkWidth = x1-x0
		area = np.float32(.5*(chunkWidth*(y1+y0)))
		return area
	for i in range(0, nTraps-1):
		a = a + oneTrapIntegrate(x[i], x[i+1], y[i], y[i+1])
	return a

x = []
y = []
Da = []
nTraps = 1000
width = np.float32((x1-x0)/nTraps)

for i in range(0,nTraps+1):
	if i == 0:
		x.append(x0)
	else:
		x.append(width*i)
	#print x[i]
	h = np.float64(3000/ ((( 0.3 * ((1+x[i])**3)) + 0.7 )**.5))
	y.append(h)

for i in range(0, nTraps+1):
	Da.append(nTrapIntegrate(nTraps,x,y)/(1+x[i]))


print "homegrown trapezoid integrator (using 1000 trapezoids): ", nTrapIntegrate(nTraps,x,y) 		
print "canned trapezoid integrator: ", np.trapz(y,x)


plt.plot(x, Da, 'g^')
#axes = plt.gca()
#axes.set_ylim([0, 2000])
plt.xlabel('z')
plt.ylabel('Da')
plt.show()

	
	
			