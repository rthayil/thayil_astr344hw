import random
import numpy as np


N = 10000000
i = 0
counter = 0.0
while (i<N):
	x = random.random()
	y = random.random()
	z = np.float64(x**2 + y**2)
	if(z) <= 1:
		counter = counter + 1
	i= i+1
print counter
print N
print counter/N * 4

#on N = order of 10^6 got 3.141 a few times
#on N = order of 10^7, got 3.141 more consistently
