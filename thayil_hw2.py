import numpy as np

for n in range(0, 21):
	s = np.float32(3**-1)
	x = np.float32(s**n)
	#print ("n:",n, "x:", x)
	if n == 5 or n == 20:
		s1 = np.float64(3**-1)
		xReal = np.float64(s1**n)
		print ("xReal = ", xReal)
		absError = np.float64(xReal-x)
		relError = np.float64(absError/xReal)
		print ("relative error at n = ", n, "is", relError)
		print ("absolute error at n = ", n, "is", absError)

for m in range(0, 21):
	y = np.float32(4**n)
	if m == 1:
		yReal = np.float64(4**n)
		absError = np.float64(yReal-y)
		relError = np.float64(absError/yReal)
		print ("relative error at m = ", m, "is", relError)
		print ("absolute error at m = ", m, "is", absError)
        
#relative error is useful for measures of accuracy while absolute error is useful for measures of stability.