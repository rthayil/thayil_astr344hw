import random
import math as m
import matplotlib.pyplot as plt
import numpy

#getting value from random exponential function
############################################
def randNum(func, xUpBound, xLowBound):
	x = random.uniform(xLowBound, xUpBound)
	y = func(x)
	return y

def fx(x):
	return m.exp(x)

#print randNum(fx, 0, 5)
##############################################

class Person(object):

	def __init__(self, x, y, infected, vaccinated):
		self.x = int(x)
		self.y = int(y)
		self.infected = infected
		self.vaccinated = vaccinated

	def move(self, roomSizeX, roomSizeY):
		moveX = random.randint(0,5)
		moveY = random.randint(0,5)
		n = random.randint(1,2)
		m = random.randint(1,2)
		directionX = (-1)**n
		directionY = (-1)**m
		#if statements to not go out of bounds
		if self.x + directionX*moveX <= roomSizeX and self.x +directionX*moveX >= 0:
			self.x = self.x + directionX*moveX
		else:
			self.x = self.x + -1*directionX*moveX
		if self.y + directionY*moveY <= roomSizeY and self.y +directionY*moveY >= 0:
			self.y = self.y + directionY*moveY
		else: self.y = self.y + -1*directionY*moveY
	
	def infect(self):
		self.infected = True
	
	def infectZone(self, person):
		dist = m.sqrt((self.x - person.x)**2 + (self.y - person.y)**2)
		if dist <= 7 :
			num = random.random()
			if(m.exp(num) > 1.15):
				person.infect()
			
class Data(object):
	def __init__(self, numPeople, numInf, numVac, clock):
		self.numPeople = numPeople
		self.numInf = numInf
		self.numVac= numVac
		self.clock = clock
	
roomSizeX = 200
roomSizeY = 200	


#clock and movement per time cycle
data = []
healthPeopleArr = []
vacPeopleArr = []
clockArr = []
w=0
while (w < 5) :
	for z in range(50, 98):
		uninfectedPeople = []
		xArr = []
		yArr = []
		infectedPeople = []
		xArrInf = []
		yArrInf = []
		xArrVac = []
		yArrVac = []
		vaccinatedPeople = []
		numPeople = z
		numInfPeople = 1
		numVacPeople = 99-z
		#print "NEW ROUND"
		#print "numPeople" , numPeople
		#print "numInfectedPeople" , numInfPeople
		#print "numVaccinatedPeople" , numVacPeople
		for i in range(0,numPeople):
			a = random.random()*roomSizeX
			b = random.random()*roomSizeY
			person = Person(a,b, False, False)
			uninfectedPeople.append(person)
			xArr.append(person.x)
			yArr.append(person.y)
			#print i, person.x
		for i in range(0, numInfPeople):
			a = random.random()*roomSizeX
			b = random.random()*roomSizeY
			person = Person(a, b, True, False)
			infectedPeople.append(person)
			xArrInf.append(person.x)
			yArrInf.append(person.y)
		for i in range(0,numVacPeople):
			a = random.random()*roomSizeX
			b = random.random()*roomSizeY
			person = Person(a, b, False, True)
			vaccinatedPeople.append(person)
			xArrVac.append(person.x)
			yArrVac.append(person.y)




		clock = 0
		while len(uninfectedPeople)>0 :
			clock = clock+1
		#for clock in range(0, timeCycles):
			del xArr[:]
			del yArr[:]
			del xArrInf[:]
			del yArrInf[:]
			del xArrVac[:]
			del yArrVac[:]
			numInfectPeople = len(infectedPeople)
			numHealthPeople = len(uninfectedPeople)
			for j in range(0, numInfectPeople):
				i = 0
				while i<numHealthPeople :
				#for i in range(0, numHealthPeople - 1):
					infectedPeople[j].infectZone(uninfectedPeople[i])
					if uninfectedPeople[i].infected == True:
						infectedPeople.append(uninfectedPeople[i])
						del uninfectedPeople[i]
						i = i-1
						numHealthPeople = numHealthPeople-1
					i = i+1
					
			for i in range(0,len(uninfectedPeople)):
				uninfectedPeople[i].move(roomSizeX, roomSizeY)
				xArr.append(uninfectedPeople[i].x)
				yArr.append(uninfectedPeople[i].y)
				#print clock, i, uninfectedPeople[i].x
			
			for j in range(0, len(infectedPeople)):
				infectedPeople[j].move(roomSizeX, roomSizeY)
				xArrInf.append(infectedPeople[j].x)
				yArrInf.append(infectedPeople[j].y)
			for k in range(0, len(vaccinatedPeople)):
				vaccinatedPeople[k].move(roomSizeX, roomSizeY)
				xArrVac.append(vaccinatedPeople[k].x)
				yArrVac.append(vaccinatedPeople[k].y)
			#plt.plot(xArr, yArr, 'go')  	#GRAPH AT EACH TIME CYCLE: I didnt know how to make an animation, comment out if you dont want to see 30 graphs
			#plt.plot(xArrInf, yArrInf, 'ro')
			#plt.plot(xArrVac, yArrVac, 'bo')
			#plt.show()
		#print clock
		roundData = Data(numPeople, numInfPeople, numVacPeople, clock)
		healthPeopleArr.append(numPeople)
		vacPeopleArr.append(numVacPeople)
		clockArr.append(clock)
		data.append(roundData)
	w = w+1
sz = len(clockArr)/5
for i in range(0, sz):
	clockArr[i] = (clockArr[i]+clockArr[i+sz]+ clockArr[i+2*sz] + clockArr[i+3*sz] + clockArr[i+4*sz])/5
del clockArr[sz:sz*5]
del vacPeopleArr[sz:sz*5]
plt.plot(vacPeopleArr, clockArr, 'go')  	
#plt.plot(xArrInf, yArrInf, 'ro')
#plt.plot(xArrVac, yArrVac, 'bo')
plt.show()

	
