import random
import matplotlib.pyplot as plt

numTrials = 100
i = 0
j = 0
match = 0
counter = 0
numPeople = 2
bdays = []
maxPeople = 100
arrX = []
arrY = []
for numPeople in range(2, maxPeople):
	match = 0
	for j in range(numTrials):
		counter = 0
		for i in range(numPeople) :
			day = int(random.random()*365)
			bdays.append(day)
		
		bdays.sort()
		for i in range(1,len(bdays)-1):	
			if bdays[i] == bdays[i-1] :
				counter = counter + 1
		#print numPeople, counter
		match = match + counter
		
		bdays = []
	avg = match/numTrials
	prob = avg/numPeople
	print numPeople , avg
	arrX.append(numPeople)
	arrY.append(avg)

plt.plot(arrX, arrY, 'g^') # note the plot is not probability but avg number of collisions

plt.xlabel('numPeople')
plt.ylabel('avg collisions')
plt.show()

#I get one collision fairly consistently around 28 people