import random
import math as m
import matplotlib.pyplot as plt

#getting value from random exponential function
############################################
def randNum(func, xUpBound, xLowBound):
	x = random.uniform(xLowBound, xUpBound)
	y = func(x)
	return y

def fx(x):
	return m.exp(x)

#print randNum(fx, 0, 5)
##############################################

class Person(object):

	def __init__(self, x, y):
		self.x = int(x)
		self.y = int(y)

	def move(self, roomSizeX, roomSizeY):
		moveX = random.randint(0,5)
		moveY = random.randint(0,5)
		n = random.randint(1,2)
		m = random.randint(1,2)
		directionX = (-1)**n
		directionY = (-1)**m
		#if statements to not go out of bounds
		if self.x + directionX*moveX <= roomSizeX and self.x +directionX*moveX >= 0:
			self.x = self.x + directionX*moveX
		else:
			self.x = self.x + -1*directionX*moveX
		if self.y + directionY*moveY <= roomSizeY and self.y +directionY*moveY >= 0:
			self.y = self.y + directionY*moveY
		else: self.y = self.y + -1*directionY*moveY
		
		
roomSizeX = 200
roomSizeY = 200	

#initializing array of people
people = []
xArr = []
yArr = []
numPeople = 100
timeCycles = 30

for i in range(0,numPeople):
	a = random.random()*roomSizeX
	b = random.random()*roomSizeY
	person = Person(a,b)
	people.append(person)
	xArr.append(person.x)
	yArr.append(person.y)
	#print i, person.x
plt.plot(xArr, yArr, 'ro')
plt.show()
print "post move"

timeCycles = 30
#clock and movement per time cycle
for clock in range(0, timeCycles):
	del xArr[:]
	del yArr[:]
	for i in range(0,numPeople):
		people[i].move(roomSizeX, roomSizeY)
		xArr.append(people[i].x)
		yArr.append(people[i].y)
		#print clock, i, people[i].x
	plt.plot(xArr, yArr, 'ro')  				#GRAPH AT EACH TIME CYCLE: I didnt know how to make an animation, comment out if you dont want to see 30 graphs
	plt.show()
	
